import React from 'react'
import Icon from '@material-ui/core/Icon'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import auth from './../auth/auth-helper'
import {Link, withRouter} from 'react-router-dom'
import logo from './../assets/images/logo.png'
import Grid from '@material-ui/core/Grid'

const isActive = (history, path) => {
  if (history.location.pathname == path)
    return {color: '#ffd369'}
  else
    return {color: '#ffffff'}
}
const Menu = withRouter(({history}) => (
  <AppBar style={{ background: '#343a40' }} position="sticky">
    <Toolbar variant="regular">
    <Grid   
      container
      direction="row"
      justify="space-between"
      alignItems="baseline"
    >
    <Grid item xs={2} style={{textAlign: 'left'}}>
    <Link to="/">
    <IconButton aria-label="Home" style={isActive(history, "/")}>
      <Icon>
        <img style={{height: '100%', textAlign: 'center'}} src={logo} alt="logo"></img>
      </Icon>
      </IconButton>
    </Link>
    </Grid>
    <Grid item xs={10} style={{textAlign: 'right'}}>
      {
        !auth.isAuthenticated() && (<span>
          <Link to="/signup">
            <Button style={isActive(history, "/signup")}>Sign up
            </Button>
          </Link>
          <Link to="/signin">
            <Button style={isActive(history, "/signin")}>Sign In
            </Button>
          </Link>
        </span>)
      }
      {
        auth.isAuthenticated() && (<span>
          <Link to={"/user/" + auth.isAuthenticated().user._id}>
            <Button style={isActive(history, "/user/" + auth.isAuthenticated().user._id)}>My Profile</Button>
          </Link>
          <Button color="inherit" onClick={() => {
              auth.clearJWT(() => history.push('/'))
            }}>Sign out</Button>
        </span>)
      }
      </Grid>
      </Grid>
    </Toolbar>
  </AppBar>
))

export default Menu
